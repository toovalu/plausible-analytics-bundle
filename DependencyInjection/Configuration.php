<?php

namespace Toovalu\PlausibleAnalyticsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('toovalu_plausible_analytics');

        $treeBuilder->getRootNode()
            ->children()
            ->booleanNode('enabled')->defaultTrue()->info('If disabled we will inject tracking code')->end()
            ->scalarNode('domain')
                ->defaultValue('http://localhost')
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('endpoint')
                ->defaultValue('https://plausible.io/js/plausible.js')
                ->cannotBeEmpty()
            ->end()->end();

        return $treeBuilder;
    }
}

