<?php

namespace Toovalu\PlausibleAnalyticsBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Toovalu\PlausibleAnalyticsBundle\EventSubscriber\PlausibleAnalyticsEventSubscriber;

class ToovaluPlausibleAnalyticsExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');
        $eventSubscriberDef = $container->getDefinition('toovalu_plausible_analytics_bundle.event_subscriber');
        if ($config['enabled']) {
            $eventSubscriberDef->setBindings([
                '$domain' => $config['domain'],
                '$endpoint' => $config['endpoint'],
            ]);
        } else {
            $container->removeDefinition('toovalu_plausible_analytics_bundle.event_subscriber');
        }
    }
}
