<?php

namespace Toovalu\PlausibleAnalyticsBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class PlausibleAnalyticsEventSubscriber implements EventSubscriberInterface
{
    public function __construct(private string $endpoint, private string $domain)
    {
    }
    public function onKernelResponse(ResponseEvent $event): void
    {
        $this->injectTrackingCode($event->getResponse());
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    private function injectTrackingCode($response)
    {
        $content = $response->getContent();
        $endHeadPos = strripos($content, '</head>');
        $hasBody = false !== $endHeadPos;

        if ($hasBody) {
            $trackingCode = sprintf(
                '<script defer data-domain="%s" src="%s"></script>',
                $this->domain,
                $this->endpoint
            );
            
            if ($endHeadPos) {
                $content = substr($content, 0, $endHeadPos) . $trackingCode .substr($content, $endHeadPos);
            } else {
                $content .= $trackingCode;
            }
            $response->setContent($content);
        }
    }
}
