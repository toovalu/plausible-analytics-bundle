# Toovalu Plausible Analytics Bundle

## Install

```bash
composer req toovalu/plausible-analytics-bundle
```

## Configuration

```yaml
#config/packages/toovalu_plausible_analytics.yaml
toovalu_plausible_analytics:
    enabled: true
    domain: example.com # required
    endpoint: https://plausible.io/js/plausible.js # default
```

🎉 And it's done, the script tag should be injected in responses
