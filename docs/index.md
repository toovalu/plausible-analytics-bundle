## Install

```bash
composer req toovalu/plausible-analytics-bundle
```

## Configuration

```yaml
#config/packages/toovalu_plausible_analytics.yaml
toovalu_plausible_analytics:
    enabled: true
    domain: impact.toovalu.com # required
    endpoint: https://plausible.io/js/plausible.js # default
```
